.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../../Includes.txt

.. _showAvatar:

Show avatar
===========

===================================== ========
Property                               Type
===================================== ========
showAvatar_                            boolean
===================================== ========

.. _showAvatar:

showAvatar
"""""""""""""""""""
.. container:: table-row

   Property
      showAvatar
   Data type
      boolean
   Default
      1
   Description
      If enabled an avatar image is displayed for each comment.
